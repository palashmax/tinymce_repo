/**
 * Copyright (c) Tiny Technologies, Inc. All rights reserved.
 * Licensed under the LGPL or a commercial license.
 * For LGPL see License.txt in the project root for license information.
 * For commercial licenses see https://www.tiny.cloud/
 */

// import DOMUtils from 'tinymce/core/api/dom/DOMUtils';
// import Tools from 'tinymce/core/api/util/Tools';
//import Settings from '../api/Settings';
import TextCase from '../core/TextCase';

const onButtonClick = function (editor) {
  return function (e) {
    // const ctrl = e.control;
    const buttonCtrl = this.parent();
    if (e.control.settings.tooltip == 'UpperCase') {
      TextCase.getCurrentColor(editor, buttonCtrl.settings.format);
    }
    else if (e.control.settings.tooltip == 'LowerCase') {
      TextCase.convertToLowerCase(editor, buttonCtrl.settings.format);
    }
    editor.fire('change',e);
  };
};

const register = function (editor) {
  editor.addButton('upperCase', {
    text:'U',
    tooltip: 'UpperCase',
    format: 'forecolor',
     panel: {
      role: 'application',
      ariaRemember: true,


    },
    onclick: onButtonClick(editor)
  });

  editor.addButton('lowerCase', {
    text:'L',
    tooltip: 'LowerCase',
    format: 'hilitecolor',
    panel: {
      role: 'application',
      ariaRemember: true,

    },
    onclick: onButtonClick(editor)
  });
};

export default {
  register
};