/**
 * Copyright (c) Tiny Technologies, Inc. All rights reserved.
 * Licensed under the LGPL or a commercial license.
 * For LGPL see License.txt in the project root for license information.
 * For commercial licenses see https://www.tiny.cloud/
 */

import TextCase from '../core/TextCase';

const register = function (editor) {
  editor.addCommand('mceApplyTextcase', function (format, value) {
    TextCase.applyFormat(editor, format, value);
  });

  editor.addCommand('mceRemoveTextcase', function (format) {
    TextCase.removeFormat(editor, format);
  });
};

export default {
  register
};
