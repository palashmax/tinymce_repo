/**
 * Copyright (c) Tiny Technologies, Inc. All rights reserved.
 * Licensed under the LGPL or a commercial license.
 * For LGPL see License.txt in the project root for license information.
 * For commercial licenses see https://www.tiny.cloud/
 */

const getCurrentColor = function (editor, format) {
  let color;
  let textContentEdit = undefined;
  if (editor.selection && editor.selection.getContent()) {
    textContentEdit = editor.selection.getContent(format, 'html');
    if (textContentEdit) {
      //textContentEdit = textContentEdit.replace(/<\/?span[^>]*>/g, "");
      textContentEdit=textContentEdit.toUpperCase();
      editor.selection.setContent(textContentEdit);
    }
  } return color;
};

const convertToLowerCase = function (editor, format) {
  let color;
  let textContentEdit = undefined;
  if (editor.selection && editor.selection.getContent()) {
    textContentEdit = editor.selection.getContent(format, 'html');
    if (textContentEdit) {
    // textContentEdit = textContentEdit.replace(/<\/?span[^>]*>/g, "");
      textContentEdit=textContentEdit.toLowerCase();
      editor.selection.setContent(textContentEdit);
    }
  } return color;
};


const applyFormat = function (editor, format, value) {
  editor.undoManager.transact(function () {
    editor.focus();
    editor.formatter.apply(format, {
      value
    });
    editor.nodeChanged();
  });
};

const removeFormat = function (editor, format) {
  editor.undoManager.transact(function () {
    editor.focus();
    editor.formatter.remove(format, {
      value: null
    }, null, true);
    editor.nodeChanged();
  });
};

export default {
  getCurrentColor,
  convertToLowerCase,
  applyFormat,
  removeFormat
};